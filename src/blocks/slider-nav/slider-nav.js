$(function () {

  const sliderWrap = '.js-slider-wrap';
  const sliderInstance = '.js-slider';
  const sliderPrevBtn = '.js-slider-prev';
  const sliderNextBtn = '.js-slider-next';
  const sliderCountCurrent = '.js-slider-current';
  const sliderCountLength = '.js-slider-length';

  $(sliderInstance).each(function(){
    const self = this;

    $(self).on('init', function (event, slick) {
      const sliderWrapNode = $(self).parents(sliderWrap);
      sliderWrapNode.find(sliderCountCurrent).html(`0${slick.currentSlide + 1}`);
      sliderWrapNode.find(sliderCountLength).html(slick.slideCount);
    });

    $(self).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      $(this).parents(sliderWrap).find(sliderCountCurrent).html(`0${nextSlide + 1}`);
    });

  });

  $(document).on('click', sliderPrevBtn, function(){
    $(this).parents(sliderWrap).find(sliderInstance).slick('slickPrev');
  })

  $(document).on('click', sliderNextBtn, function(){
    $(this).parents(sliderWrap).find(sliderInstance).slick('slickNext');
  })

})