import slick from 'slick-carousel';

$(function () {
  // PROMO SLIDER
  $('.js-promo-slider').slick({
    infinite: true,
    autoplay: true,
    autoplaySpeed: 3000,
    adaptiveHeight: true,
    arrows: false,
    dots: false,
    fade: true,
    speed: 1500
  });
})