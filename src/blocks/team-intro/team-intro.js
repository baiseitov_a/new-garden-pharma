import slick from 'slick-carousel';

$(function () {

  $('.js-team-intro').each(function (index, element) {
    const contentSlider = $(element).find('.js-team-content-slider');
    const imagesSlider = $(element).find('.js-team-images-slider');
    const thumbSlider = $(element).find('.js-team-thumb-slider');

    function getClassNames(node){
      node.addClass(`prefix-slider-${index}`);
      return '.' + node.attr("class").split(/\s+/).join(".")
    }

    $(contentSlider).slick({
      autoplay: true,
      autoplaySpeed: 3000,
      infinite: false,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      speed: 500,
      adaptiveHeight: true,
      asNavFor: `${getClassNames(imagesSlider)}, ${getClassNames(thumbSlider)}`
    });

    $(imagesSlider).slick({
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      speed: 500,
      asNavFor: `${getClassNames(thumbSlider)}, ${getClassNames(contentSlider)}`,
    });

    $(thumbSlider).slick({
      arrows: false,
      infinite: false,
      slidesToShow: 6,
      slidesToScroll: 1,
      asNavFor: `${getClassNames(imagesSlider)}, ${getClassNames(contentSlider)}`,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1
          }
        }
      ]
    });

  })
})