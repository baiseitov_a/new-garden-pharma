import '@root/scss/index.scss';
import '@root/js/common';

import $ from 'jquery';
import slick from 'slick-carousel';

import '@root/blocks/promo/promo';
import '@root/blocks/team-intro/team-intro';