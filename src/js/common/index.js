import $ from 'jquery';
import select2 from 'select2';
import viewportChecker from 'jquery-viewport-checker';

import '@root/layout/header/header';
import '@root/blocks/slider-nav/slider-nav';

import './tab';

$(document).ready(function () {

  $('.js-anim').viewportChecker();

  //custom select styling
  $('.js-select').select2({
    minimumResultsForSearch: -1
  });

  
});