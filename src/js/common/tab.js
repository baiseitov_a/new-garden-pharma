$(function() {

  // tabs
  $('.js-tab-navlink').click(function (e) {
    e.preventDefault();
    const id = $(this).attr('href');
    const parentNode = $(this).parents('.js-tab');
    parentNode.find('.js-tab-navlink').removeClass('active');
    $(this).addClass('active');
    parentNode.find('.js-tab-block').removeClass('active');
    $(id).addClass('active');
  });

})