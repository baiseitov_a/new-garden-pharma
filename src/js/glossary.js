import '@root/scss/glossary.scss';
import '@root/js/common';

import $ from 'jquery';

$(function(){
  const $glossaryNav = $('.js-glossary-nav');
  const isHideableList = $glossaryNav.find('.js-glossary-navitem').length > 8;

  if (isHideableList) {
    $glossaryNav.addClass('is-toggleable is-hidden');
  }

  $('.js-glossary-show').click(function (e) {
    e.preventDefault();
    $glossaryNav.removeClass('is-hidden').addClass('is-showed');
  })
  $('.js-glossary-hide').click(function (e) {
    e.preventDefault();
    $glossaryNav.removeClass('is-showed').addClass('is-hidden');
  })

  $('.js-glossary-list')
    .find('.special-block')
    .not('.special-block--revert')
    .filter(":odd")
    .addClass('left-img-is-big');

  $('.js-glossary-link').click(function (e) {
    e.preventDefault();
    const glossaryId = $(this).attr('href');

    $('html, body').animate({
      scrollTop: $(glossaryId).offset().top
    }, 500);
    return false;    
  });
  
})